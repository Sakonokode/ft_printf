NO_COLOR=\033[0m
OK_COLOR=\033[32;01m
ERROR_COLOR=\033[31;01m
IN_PROGRESS_COLOR=\033[33;01m

PRINTF = ft_printf

NAME = $(PRINTF)

LIBFT = libft.a

CFLAGS = -Wall -Werror -Wextra

CC = gcc

RM = rm -rf

SRCDIR = ./srcs
OBJDIR = ./srcs

SRCS=ft_printf.c\
	 init_data.c\
	 main.c\
	 printf_adds.c\
	 printf_colors.c\
	 printf_fills.c\
	 set_flags.c\
	 convertions_tools/base_nb_length.c\
	 convertions_tools/charw_to_char.c\
	 convertions_tools/conv_c_strnew.c\
	 convertions_tools/conv_f_memjoin.c\
	 convertions_tools/conv_f_strjoin.c\
	 convertions_tools/conv_f_strsub.c\
	 convertions_tools/conv_f_to_conv_a.c\
	 convertions_tools/conv_l_itoa.c\
	 convertions_tools/memjoin.c\
	 convertions_tools/ft_power.c\
	 convertions_tools/ft_str_toupper.c\
	 convertions_tools/ft_strwlen.c\
	 convertions_tools/strtonewstr.c\
	 convertions_tools/wchar_length.c\
	 convertions_tools/wstr_to_str.c\
	 convertions/conv_a.c\
	 convertions/conv_b.c\
	 convertions/conv_c.c\
	 convertions/conv_e.c\
	 convertions/conv_f.c\
	 convertions/conv_g.c\
	 convertions/conv_i.c\
	 convertions/conv_n.c\
	 convertions/conv_o.c\
	 convertions/conv_p.c\
	 convertions/conv_s.c\
	 convertions/conv_u.c\
	 convertions/conv_x.c

OBJS = $(addprefix $(OBJDIR)/, $(SRCS:.c=.o))

all: $(NAME)
	
$(NAME): $(LIBFT) $(OBJS)
	@echo "Compilation of printf :$(IN_PROGRESS_COLOR) [IN PROGRESS] $(NO_COLOR)"
	@$(CC) $(CFLAGS) -Llibft $(OBJS) -lft -o $(NAME)
	@$(CC) $(CFLAGS) -Llibft $(OBJS) -lft -g -o POURGDB
	@echo "$(SRCS)"

$(addprefix $(OBJDIR)/, %.o): $(addprefix $(SRCDIR)/, %.c)
	@mkdir -p $(OBJDIR)
	@$(CC) $(CFLAGS) -o $@ -c $^

$(LIBFT):
	@echo "Compilation of Libft :$(IN_PROGRESS_COLOR) [IN PROGRESS] $(NO_COLOR)"
	@make -C ./libft/

clean:
	@$(RM) $(OBJS)
	@make clean -C ./libft/

fclean: clean
	@$(RM) $(LIBFT)
	@$(RM) $(NAME)
	@$(RM) POURGDB
	@make fclean -C ./libft/

re: fclean all

.PHONY: fclean all re clean

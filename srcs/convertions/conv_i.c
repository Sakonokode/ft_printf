#include "../../includes/ft_printf.h"

void    conv_uc_d(t_data *w, t_conv *ptr)
{
    long long int   get_data;

    get_data = CALL_VA_ARG(w->ap, long int);
    if (get_data < 0)
    {
        ptr->is_neg = true;
        ptr->nb = get_data * -1;
    }
    else
        ptr->nb = get_data;
    add_i(ptr);
}

void    conv_i(t_data *d, t_conv *ptr)
{
    long long int   get_data;

    if (ptr->is_long_long)
        get_data = CALL_VA_ARG(d->ap, long long int);
    else if (ptr->is_long)
        get_data = CALL_VA_ARG(d->ap, long int);
    else if (ptr->is_char)
        get_data = (char)CALL_VA_ARG(d->ap, int);
    else if (ptr->is_short)
        get_data = (short int)CALL_VA_ARG(d->ap, int);
    else
        get_data = CALL_VA_ARG(d->ap, int);
    if (get_data < 0)
    {
        ptr->is_neg = true;
        ptr->nb = get_data * -1;
    }
    else
        ptr->nb = get_data;
    add_i(ptr);
}

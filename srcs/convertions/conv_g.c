#include "../../includes/ft_printf.h"

static void     set_double(t_conv *ptr)
{
    short int   pow;
    long double n_ptr;

    pow = 0;
    n_ptr = ptr->dble;
    while (n_ptr >= 10.0 && ++pow)
        n_ptr /= 10;
    while (n_ptr < 1.0 && --pow)
        n_ptr *= 10;
    if (pow < 6 && pow <= ptr->accuracy)
    {
        ptr->accuracy -= (pow);
        if (ptr->accuracy < 0)
            ptr->accuracy = 0;
        add_f(ptr);
        while (ptr->str[ptr->len - 1] == '0')
            ptr->str[--ptr->len] = '\0';
        if (ptr->str[ptr->len - 1] == '.')
            ptr->str[--ptr->len] = '\0';
    }
    else
        add_e(ptr);
}

void            conv_uc_g(t_data *d, t_conv *ptr)
{
    ptr->spec = 'F';
    if (!ptr->accuracy)
        ptr->accuracy = 1;
    if (ptr->accuracy == -1)
        ptr->accuracy = 6;
    --ptr->accuracy;
    if (ptr->is_long)
        ptr->dble = CALL_VA_ARG(d->ap, long double);
    else
        ptr->dble = CALL_VA_ARG(d->ap, double);
    if (ptr->dble < 0)
    {
        ptr->dble = -1 * ptr->dble;
        ptr->is_neg = true;
    }
    set_double(ptr);
}

void            conv_g(t_data *d, t_conv *ptr)
{
    ptr->spec = 'f';
    if (!ptr->accuracy)
        ptr->accuracy = 1;
    if (ptr->accuracy == -1)
        ptr->accuracy = 6;
    --ptr->accuracy;
    if (ptr->is_long)
        ptr->dble = CALL_VA_ARG(d->ap, long double);
    else
        ptr->dble = CALL_VA_ARG(d->ap, double);
    if (ptr->dble < 0)
    {
        ptr->dble = -1 * ptr->dble;
        ptr->is_neg = true;
    }
    set_double(ptr);
}

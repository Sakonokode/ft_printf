#include "../../includes/ft_printf.h"

void    conv_p(t_data *d, t_conv *ptr)
{
    void    *tmp;

    tmp = CALL_VA_ARG(d->ap, void *);
    if (!tmp && !ptr->accuracy)
    {
        --(ptr->accuracy);
        ptr->len += 2;
        add_s(ptr, ft_strdup("0x"));
    }
    else
    {
        ptr->base = 16;
        ptr->spec = 'x';
        ptr->sha = true;
        ptr->nb = (unsigned long int)tmp;
        add_i(ptr);
    }
}

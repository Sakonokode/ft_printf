#include "../../includes/ft_printf.h"

static void add_a(t_conv *ptr)
{
    short int   len;
    short int   pow;
    char        *mystr;

    pow = 0;
    while (ptr->dble >= 2.0 && ++pow)
        ptr->dble /= 2;
    while (ptr->dble < 1.0 && --pow)
        ptr->dble *= 2;
    ptr->str = ft_ftoa_base(ptr->dble, ptr->accuracy, ptr->base, ptr->spec == 'X');
    len = ft_strlen(ptr->str);
    mystr = ft_strnewc(ptr->accuracy - len + 2, '0');
    ptr->str = ft_strjoinf(ptr->str, mystr, 3);
    ptr->accuracy = 0;
    ptr->width = ptr->width - 2 - base_nb_length(pow, 10);
    fill_numbers(ptr, len);
    if (ptr->spec == 'x')
        mystr = ft_strjoin(pow < 0 ? "p" : "p+", ft_itoa(pow));
    else
        mystr = ft_strjoin(pow < 0 ? "P" : "P+", ft_itoa(pow));
    ptr->str = ft_strjoinf(ptr->str, mystr, 3);
    ptr->len = ft_strlen(ptr->str);
}

static void set_double(t_data *d, t_conv *ptr)
{
    ptr->base = 16;
    ptr->sha = true;
    if (ptr->accuracy == -1)
        ptr->accuracy = 13;
    if (ptr->is_long)
        ptr->dble = CALL_VA_ARG(d->ap, long double);
    else
        ptr->dble = CALL_VA_ARG(d->ap, double);
    if (ptr->dble < 0)
    {
        ptr->dble = -1 * ptr->dble;
        ptr->is_neg = true;
    }
    add_a(ptr);
}

void        conv_uc_a(t_data *d, t_conv *ptr)
{
    ptr->spec = 'X';
    set_double(d, ptr);
}

void        conv_a(t_data *d, t_conv *ptr)
{
    ptr->spec = 'x';
    set_double(d, ptr);
}

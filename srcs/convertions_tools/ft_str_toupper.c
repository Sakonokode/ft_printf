#include "../../includes/ft_printf.h"

char    *ft_strtoupper(char *str)
{
    char    *s;

    if (!str)
        return (NULL);
    s = str;
    while (*str)
    {
        *str = ft_toupper(*str);
        ++str;
    }
    return (s);
}

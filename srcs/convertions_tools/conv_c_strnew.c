#include "../../includes/ft_printf.h"

char    *ft_strnewc(size_t size, int c)
{
    char *sptr;

    if (!(sptr = (char*)malloc(sizeof(char) * (size + 1))))
        return (NULL);
    ft_memset((void*)sptr, c, size);
    sptr[size] = '\0';
    return (sptr);
}

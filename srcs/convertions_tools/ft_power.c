#include "../../includes/ft_printf.h"

long long int       ft_pow(int nb, int power)
{
    int                 cpt;
    long long int       value;

    cpt = 1;
    value = nb;
    if (power > 0)
        while (cpt++ < power)
            value = value * nb;
    else
    {
        if (power == 0)
            value = 1;
        else
            value = 0;
    }
    return (value);
}

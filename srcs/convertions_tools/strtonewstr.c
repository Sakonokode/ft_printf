#include "../../includes/ft_printf.h"

char    *strwtonewstr(const wchar_t *strw)
{
    char    *str;

    if (!strw)
        return (NULL);
    if (!(str = (char*)malloc(sizeof(char) * (ft_strwlen(strw) + 1))))
        return (NULL);
    return (ft_strwtostr(str, strw));
}

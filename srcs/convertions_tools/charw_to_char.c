#include "../../includes/ft_printf.h"

char    *ft_charwtochar(char *str, const wchar_t cw)
{
    char *s;

    s = str;
    if (cw <= 0x7F)
        *s = cw;
    else if (cw <= 0x7FF)
    {
        *s = (cw >> 6) + 0xC0;
        *(++s) = (cw & 0x3F) + 0x80;
    }
    else if (cw <= 0xFFFF)
    {
        *s = (cw >> 12) + 0xE0;
        *(++s) = ((cw >> 6) & 0x3F) + 0x80;
        *(++s) = (cw & 0x3F) + 0x80;
    }
    else if (cw <= 0x10FFFF)
    {
        *s = (cw >> 18) + 0xF0;
        *(++s) = ((cw >> 12) & 0x3F) + 0x80;
        *(++s) = ((cw >> 6) & 0x3F) + 0x80;
        *(++s) = (cw & 0x3F) + 0x80;
    }
    *(++s) = '\0';
    return (str);
}

char    *charw_to_newchar(const wchar_t cw)
{
    char    *c;

    if (!(c = (char*)malloc(sizeof(char) * (charwlen(cw) + 1))))
        return (NULL);
    return (ft_charwtochar(c, cw));
}

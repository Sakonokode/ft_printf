#include "../includes/ft_printf.h"

static void         initialize_fun_bonus(t_data *d)
{
    d->conv_func[b] = &conv_b;
    d->conv_func[uc_b] = &conv_uc_b;
    d->conv_func[e] = &conv_e;
    d->conv_func[uc_e] = &conv_uc_e;
    d->conv_func[f] = &conv_f;
    d->conv_func[uc_f] = &conv_uc_f;
    d->conv_func[g] = &conv_g;
    d->conv_func[uc_g] = &conv_uc_g;
    d->conv_func[a] = &conv_a;
    d->conv_func[uc_a] = &conv_uc_a;
    d->conv_func[n] = &conv_n;
    d->conv_func[ukn] = &conv_unknown;
}

static void         initialize_fun(t_data *data)
{
    data->conv_func[s] = &conv_s;
    data->conv_func[uc_s] = &conv_uc_s;
    data->conv_func[p] = &conv_p;
    data->conv_func[d] = &conv_i;
    data->conv_func[uc_d] = &conv_uc_d;
    data->conv_func[i] = &conv_i;
    data->conv_func[o] = &conv_o;
    data->conv_func[uc_o] = &conv_uc_o;
    data->conv_func[u] = &conv_u;
    data->conv_func[uc_u] = &conv_uc_u;
    data->conv_func[x] = &conv_x;
    data->conv_func[uc_x] = &conv_uc_x;
    data->conv_func[c] = &conv_c;
    data->conv_func[uc_c] = &conv_uc_c;
}

t_data              *init_data(const char *str)
{
    t_data  *d;

    if (!(d = (t_data *)malloc(sizeof(t_data))))
        exit(1);
    d->buffer = (char *)str;
    d->ender = NULL;
    d->len = 0;
    d->curlen = 0;
    initialize_fun(d);
    initialize_fun_bonus(d);
    return (d);
}

#include "../includes/ft_printf.h"

static void         add_buffer(t_data *d, char **buffer, int len, int boolean)
{
    char    *ptr;

    if (len + d->len > d->curlen)
    {
        if (!(ptr = (char *)malloc(sizeof(char) * (d->curlen + len + 2048))))
            exit(1);
        if (d->curlen)
        {
            ft_memcpy(ptr, d->ender, d->curlen);
            ft_strdel(&d->ender);
        }
        else
            ptr[0] = '\0';
        d->ender = ptr;
        d->curlen += len + 2048;
    }
    ft_memcpy(d->ender + d->len, *buffer, len);
    if (boolean)
        ft_strdel(buffer);
}

static void         parse_options(t_data *d)
{
    t_conv  ptr;
    char    *end;

    while ((end = ft_strchr(d->buffer, '%')))
    {
        ptr.len = end - d->buffer;
        add_buffer(d, &d->buffer, ptr.len, false);
        d->len += ptr.len;
        ++end;
        set_flag(d, &ptr, &end);
        if (ptr.conv_index >= 0)
            d->conv_func[ptr.conv_index](d, &ptr);
        d->buffer = end;
        add_buffer(d, &(ptr.str), ptr.len, true);
        d->len += ptr.len;
    }
    ptr.len = ft_strlen(d->buffer);
    add_buffer(d, &(d->buffer), ptr.len + 1, false);
    d->len += ptr.len;
}

int                 ft_printf(const char *str, ...)
{
    t_data  *d;
    int     to_return;

    if (!str)
        return (0);
    d = init_data(str);
    va_start(d->ap, str);
    parse_options(d);
    d->buffer = d->ender;
    handle_colors(d);
    va_end(d->ap);
    to_return = write(1, d->ender, d->len);
    ft_strdel(&(d->ender));
    ft_memdel((void**)&d);
    return (to_return);
}

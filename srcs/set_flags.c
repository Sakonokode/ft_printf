#include "../includes/ft_printf.h"

static void         init_convertion(t_conv *ptr)
{
    ptr->conv_index = -1;
    ptr->is_neg = false;
    ptr->nb = 0;
    ptr->dble = 0;
    ptr->len = 0;
    ptr->base = 10;
    ptr->sha = false;
    ptr->spa = false;
    ptr->left = false;
    ptr->sign = false;
    ptr->is_long_long = false;
    ptr->is_long = false;
    ptr->is_short = false;
    ptr->is_char = false;
    ptr->width = 0;
    ptr->accuracy = -1;
    ptr->pad = ' ';
    ptr->spec = '\0';
}


static int          check_modifier(t_conv *ptr, char **buffer)
{
    if (**buffer == 'h' && *(1 + *buffer) == 'h')
    {
        ++(*buffer);
        return ((ptr->is_char = true));
    }
    if (**buffer == 'h')
        return ((ptr->is_short = true));
    if (**buffer == 'l' && *(1 + *buffer) == 'l')
    {
        ++(*buffer);
        return ((ptr->is_long_long = true));
    }
    if (**buffer == 'l')
        return ((ptr->is_long = true));
    if (**buffer == 'j' || **buffer == 'z')
    {
        ptr->is_long_long = false;
        return ((ptr->is_long = true));
    }
    if (**buffer == 'L')
        return ((ptr->is_long_long = true));
    return (false);
}

static int          check_accuracy(t_data *d, t_conv *ptr, char **buffer)
{
    if (**buffer == '.')
    {
        ++(*buffer);
        if (**buffer == '*')
        {
            ptr->accuracy = CALL_VA_ARG(d->ap, int);
            if (ptr->accuracy < -1)
                ptr->accuracy = -1;
            return (true);
        }
        if (ft_is_digit(**buffer))
        {
            ptr->accuracy = ft_atoi(*buffer);
            *buffer += base_nb_length(ptr->accuracy, 10) - 1;
            return (true);
        }
        --(*buffer);
        ptr->accuracy = 0;
        return (true);
    }
    return (check_modifier(ptr, buffer));
}

static int          parse_flag_and_modifier(t_data *d, t_conv *ptr, char **buffer)
{
    if (!**buffer)
        return (false);
    if (**buffer == '-')
    {
        ptr->pad = ' ';
        return ((ptr->left = true));
    }
    if (**buffer == '+')
        return ((ptr->sign = true));
    if (**buffer == ' ')
        return ((ptr->spa = true));
    if (**buffer == '#')
        return ((ptr->sha = true));
    if (**buffer == '*')
        return ((ptr->width = CALL_VA_ARG(d->ap, int)) ? true : true);
    if (**buffer == '0')
        return (!ptr->left ? ptr->pad = '0' : true);
    if (ft_is_digit(**buffer))
    {
        ptr->width = ft_atoi(*buffer);
        *buffer += base_nb_length(ptr->width, 10) - 1;
        return (true);
    }
    return (check_accuracy(d, ptr, buffer));
}

void                set_flag(t_data *d, t_conv *ptr, char **buffer)
{
    static char *flags = "sSpdDioOuUxXcCbBeEfFgGaAn";
    short int   index;

    init_convertion(ptr);
    while (parse_flag_and_modifier(d, ptr, buffer))
        ++(*buffer);
    index = 0;
    while (flags[index] && flags[index] != **buffer)
        ++index;
    ptr->conv_index = index;
    if (index == ukn)
        ptr->str = ft_strnewc(1, **buffer);
    if (**buffer)
        ++(*buffer);
    if (ptr->width < 0)
    {
        ptr->left = true;
        ptr->width = -1 * ptr->width;
    }
}

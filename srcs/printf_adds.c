#include "../includes/ft_printf.h"

static char     *create_suffixe(short int pow, char cmp)
{
    char    *suffixe;

    if (pow < 0)
    {
        pow = -1 * pow;
        suffixe = ft_strjoin(cmp == 'f' ? "e" : "E", "-");
    }
    else
        suffixe = ft_strjoin(cmp == 'f' ? "e" : "E", "+");
    if (pow > 10)
        suffixe = ft_strjoinf(suffixe, ft_itoa(pow), 3);
    else
    {
        suffixe = ft_strjoinf(suffixe, "0", 1);
        suffixe = ft_strjoinf(suffixe, ft_itoa(pow), 3);
    }
    return (suffixe);
}

void            add_e(t_conv *ptr)
{
    short int   len;
    short int   pow;

    pow = 0;
    while (ptr->dble >= 10.0 && ++pow)
        ptr->dble /= 10;
    while (ptr->dble < 1.0 && --pow)
        ptr->dble *= 10;
    ptr->str = ft_ftoa(ptr->dble, ptr->accuracy);
    len = ft_strlen(ptr->str);
    ptr->accuracy = 0;
    ptr->width -= 4;
    fill_numbers(ptr, len);
    ptr->str = ft_strjoinf(ptr->str, create_suffixe(pow, ptr->spec), 3);
    ptr->len = ft_strlen(ptr->str);
}

void            add_f(t_conv *ptr)
{
    short int   len;

    ptr->str = ft_ftoa(ptr->dble, ptr->accuracy);
    len = ft_strlen(ptr->str);
    ptr->accuracy = 0;
    fill_numbers(ptr, len);
    ptr->len = ft_strlen(ptr->str);
}

void            add_i(t_conv *ptr)
{
    short int       len;

    if (ptr->accuracy < 0)
        ptr->accuracy = ptr->base == 8 && !ptr->nb ? ptr->accuracy : 1;
    else
        ptr->pad = ' ';
    if (!ptr->accuracy && !ptr->nb)
        ptr->str = ft_strnewc(1, 0);
    else
        ptr->str = ft_ulitoa_base(ptr->nb, ptr->base, ptr->spec == 'X');
    len = ft_strlen(ptr->str);
    if (ptr->accuracy != -1)
        ptr->accuracy = ptr->accuracy < len ? 0 : ptr->accuracy - len;
    fill_numbers(ptr, len);
    ptr->len = ft_strlen(ptr->str);
}

void            add_s(t_conv *ptr, char *in)
{
    int     len;

    len = ptr->len ? ptr->len : (int)ft_strlen(in);
    if (ptr->accuracy != -1 && len > ptr->accuracy)
    {
        in = ft_strsubf(in, 0, ptr->accuracy, 1);
        len = ptr->accuracy;
    }
    fill_c(&in, ptr, ptr->width - len);
    ptr->len = len > ptr->width ? len : ptr->width;
    ptr->str = in;
}
